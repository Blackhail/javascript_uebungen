'use strict';

// isString /////////////////////////////////////////////////////////////////
document.write('Schreiben Sie eine Funktion die prüft ob der übergeben Wert eine Zeichenkette ist. Berücksichtigen Sie bitte den elementaren Datentypen String und den Objekt-String. <br><br>');

function isString(x) {
    return Object.prototype.toString.call(x) === "[object String]"
    // Object.prototype.toBlablabla >>> erstellt den prototypen eines Objektes
    // call - noch mal erklären lassen
}
var object = new String('huhu');
var array = ['huhu'];
var string = 'huhu';
var int = 5;
var bool = true;


var blabla = 'huhu';
console.log(Object.prototype.toString.call(blabla));


//////// Funktionstests: /////////
console.log(isString(object));
console.log(isString(array));
console.log(isString(string));
console.log(isString(int));
console.log(isString(bool));

if (isString(array)) {
    console.log('is a string');
}
else {
    console.log('not a string'); 
}
//////////////////////////////////
if (isString(string)) {
    console.log('is a string');
}
else {
    console.log('not a string'); 
}
//////////////////////////////////
if (isString(int)) {
    console.log('is a string');
}
else {
    console.log('not a string'); 
}
//////////////////////////////////
if (isString(bool)) {
    console.log('is a string');
}
else {
    console.log('not a string'); 
}

/////// Ende Funktionstests///////
document.write('<hr>');

// isBlanck /////////////////////////////////////////////////////////////////
document.write('Schreiben Sie eine Funktion die prüft ob ein übergebener Parameter eine leere Zeichenkette (string) ist. <br><br>');
var test1 = '';
var test2 = 'blabla';
var test3 = ['blabla' , 'blublublu'];

// wenn ausschließlich string getestet wird
    if (test1.length == 0) {
        document.write('test1: ' + 'String ist leer.');
     } else {
        document.write('test1: ' + 'String nicht leer.');
     }

     document.write('<br>');

     if (test2.length == 0) {
        document.write('test2: ' + 'String ist leer. <br>');
     } else {
        document.write('test2: ' + 'String nicht leer. <br>');
    }

// prüft Typ und Inhalt - egal weis reingegeben wird:
    if (test1 === '') {
        document.write('test1: ' + 'Leerer String! <br>');
     } else {
        document.write('test1: ' + 'Kein leerer string oder anderer Datentyp! <br>');
    }
     if (test2 === '') {
        document.write('test2: ' + 'Leerer String! <br>');
     } else {
        document.write('test2: ' + 'Kein leerer string oder anderer Datentyp! <br>');
    }  
     if (test3 === '') {
        document.write('test3: ' + 'Leerer String! <br>');
     } else {
        document.write('test3: ' + 'Kein leerer string oder anderer Datentyp! <br>');
    }

// prüft auf undefined, null oder leer:
    //  if (!!test2) {
    //     document.write('Leerer String!');
    //  }

function isBlanck (string) {
    if (string === '') {
        return 'Leerer String!';
     } else {
        return 'Kein leerer String oder anderer Datentyp!';
    }
}

// Funktionstest:
var test4 = '';
var test5 = ['blabla'];
var isstring4 = isBlanck(test4);
var isstring5= isBlanck(test5);
document.write(isstring4 , '<br>' , isstring5);

document.write('<hr>');

// truncateString /////////////////////////////////////////////////////////////////
document.write('Schreiben Sie eine Funktion die eine bestimmte vordefinierte Anzahl aus Zeichen aus einem String ausschneidet.  <br><br>');

document.write('noch nix gecodet');


document.write('<hr>');

// abName /////////////////////////////////////////////////////////////////
document.write('Schreiben Sie eine Funktion die den Nachnamen in einem übergebenen String abkürzt.  <br><br>');
var geralt = 'Gwynbleidd';
var res = geralt.slice(0, 1);
console.log(res);

var name = 'Hans Werner Ballin';
document.write('Ausgangs-String: ' + name + '<br>');

function abName (string) {

var nameArray = string.split(' ');
console.log(nameArray);                                     // in array umwandeln, Trennung bei Leerzeichen
var nameLast = (nameArray.pop().slice(0, 1) + '.');         // letztes item in Variable speichern, alle Zeichen außer erstes entfernen
                                                            // nameArray ist um das letzte item verkürzt
console.log(nameArray.join(' ') + ' ' + nameLast);          // Ausgabe Vorname(n) + abgekürzter Familienname     

return (nameArray.join(' ') + ' ' + nameLast);
}

document.write('<br> Test und Beispiele <br>');
var kurzname = abName(name);
document.write(kurzname + '</br>');
var kurzname2 = abName('Stefan-Jochen Willeke-Müller');
document.write(kurzname2 + '</br>');

document.write('<hr>');

// protEmail /////////////////////////////////////////////////////////////////////////////////////
document.write('Schreiben Sie eine Funktion die eine übergebene E-Mail verschleiert.  <br><br>');

/////// Var 1 - so gehts nicht - Gibt es passende Methode hierfür? ///////////////////////////////

/// es gibt keine Methode, um das extrahierte zu entfernen und den Rest zu behalten!!! ///////////

// var mail = 'wolverine@xmen.de'; // input
// console.log(mail.indexOf('@'));
// var pos = mail.indexOf('@');

// var start = Math.ceil(pos / 2);
// console.log(start);

// document.write('Index von @: ' + pos + '<br>');

// var fragment = mail.substr(start, Math.floor(pos / 2)) + '...';  // substr gibt ausgeschnittene Zeichenkette aus - also andere Methode suchen!
// document.write('obscured email: ' + fragment);
// console.log(fragment);

// document.write('<hr>');

/////// Var 2 - Entwurf - zwei Teile extrahieren und neu zusammenfügen /////////////////////////////////////

var mail = 'wolverine@xmen.de'; // input
console.log(mail.indexOf('@'));
var pos = mail.indexOf('@');

var start = Math.ceil(pos / 2);
console.log(start);

document.write('Index von @: ' + pos + '<br>');

var fragment_left = mail.substr(0, Math.ceil(pos / 2)) + '...';
document.write('left part: ' + fragment_left + '<br>');
console.log(fragment_left);
console.log(mail);

var fragment_right = mail.substr(pos);
document.write('right part: ' + fragment_right + '<br>');
console.log(fragment_right);
console.log(mail);

document.write('obscured mail: ' + fragment_left + fragment_right + '<br>');

document.write('<hr>');

/////// Var 2 als Funktion ///////////////////////////////////////////////////////////////////////////

var mail = 'wolverine@xmen.de'; // input

function protEmail (mail) {
    var pos = mail.indexOf('@');
    var endleft = Math.ceil(pos / 2);
    var fragment_left = mail.substr(0, endleft) + '...';
    var fragment_right = mail.substr(pos);
    return(fragment_left + fragment_right);
}
// Funktionstests:
var emailtest1 = protEmail('wolverine@x-men.de');
document.write('wolverine@x-men.de: <br>' + emailtest1 + '<br>');
var emailtest2 = protEmail('otto@gmx.de');
document.write('otto@gmx.de: <br>' + emailtest2 + '<br>');


document.write('<hr>');

/////// alphabetizeOrder ///////////////////////////////////////////////////////////////////////////////

document.write('Schreiben Sie eine Funktion die den übergebenen String alphabetisch sortiert und zurückgibt. <br><br>');

var testword = 'Gwynbleidd';
document.write(testword + '<br>');

// in array wandeln, sortieren, zusammenführen >>> ausgeben
function alphabetizeOrder(string) {
    return string.split('').sort().join('').trim('');
};

console.log(alphabetizeOrder(testword));
document.write(alphabetizeOrder(testword));

document.write('<hr>');

/////// isUpperCase() - Ist der erste Buchstabe groß? /////////////////////////////////////////////////////

document.write('<h4>Schreiben Sie eine Programm, das testet ob der erste Buchstabe einer Zeichenkette ein Großbuchstabe ist.</h4>');

// [A-Z] findet alle Charaktere im string, die ein Großbuchstabe sind
// ^[A-Z] findet den ersten Charakter des gesamten strings, wenn er ein Großbuchstabe ist, ansonsten nix

var example3 = 'ein ring sie zu knechten sie alle zu finden ins dunkel zu treiben und ewig zu binden';
function isUpperCase(string) {
    return /^[A-Z]/.test(string);
}
document.write ('Beispiel-String: ' + example3 + ': <br>' + '<b>' + isUpperCase(example3) + '</b><br><br>');

var example1 = 'weit hinten, hinter den Wortbergen, fern der Länder Vokalien und Konsonantien leben die Blindtexte <br>';
document.write(example1 + "Starts with capital letter? " + '<b>' + /^[A-Z]/.test(example1) + '</b><br>');
var example2 = 'Weit hinten, hinter den Wortbergen, fern der Länder Vokalien und Konsonantien leben die Blindtexte. <br>';
document.write(example2 + "Starts with capital letter? " + '<b>' + /^[A-Z]/.test(example2) + '</b><br>');


console.log("Treffer?" + /^[A-Z]/.test(example1));
console.log("Treffer?" + /^[A-Z]/.test(example2));


document.write('<hr>');

/////// trimStr - String von Leerzeichen bereinigen. /////////////////////////////////////////////////////
// title = title.replace(/(^[\s]+|[\s]+$)/g, '');

document.write('Schreiben Sie ein Programm das mit regulären Ausdrücken die vordefinierte Methode trim nachbildet. <br><br>');

var trim = '  hier ist ein string  ';
function trimStr(string) {
    return string.replace(/(^[\s]+|[\s]+$)/g, '');
}
var result = trimStr(trim);

// Funktionstests:
document.write ('Beispiel-String: ' + trim + '</b><br>');
document.write ('Ergebnis Beispiel-String: ' + result + '</b><br><br>');
console.log ('Beispiel-String: ' + trim + '</b><br>');
console.log ('Ergebnis Beispiel-String: ' + result + '</b><br><br>');

document.write('<hr>');

/////// isHexColor - Handelt es ich um einen Hexwert /////////////////////////////////////////////////////

document.write('Handelt es ich um einen Hexwert? <br><br>');

var hexwert = '#ffffff';

function isHexColor(string) {
    return /^#?([a-fA-F0-9]{6}|[a-fA-F0-9]{3})$/.test(string); //funzt nur bei einem Testwert
}
document.write ('Beispiel-String: ' + hexwert + ': <br>' + '<b>' + isHexColor(hexwert) + '</b><br><br>');

document.write('<hr>');

/////// removeWhitespaces - Mehr als ein Whitespace entfernen. ////////////////////////////////////////////

document.write('Schreiben Sie ein Funktion die alle Whitespaces bis auf 1 entfernt. <br><br>');

document.write('<hr>');

/////// countWords - Wörter zählen mit Vorbereitung. ////////////////////////////////////////////

document.write('Schreiben Sie ein Programm das alle Wörter innerhalb einer Zeichenkette zählt und die Anzahl zurückgibt. <br><br>');



