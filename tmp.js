'use strict';

function tage(monat){
    var monate = new Array();
    var heute = new Date();
    for(var i=heute.getFullYear();i<=heute.getFullYear()+1;i++){
        if(i%4 == 0 && (i%100 != 0 || i%400 == 0)){ //Schaltjahr wenn jahr durch 4, nicht aber durch 100 ausser durch 400
            monate[i] = {1:31,2:29,3:31,4:30,5:31,6:30,7:31,8:31,9:30,10:31,11:30,12:31};
        } else{ //kein Schaltjahr
            monate[i] = {1:31,2:28,3:31,4:30,5:31,6:30,7:31,8:31,9:30,10:31,11:30,12:31};
        }
    }
    aktu_monat = new Date(heute.getFullYear(),monat,monate[heute.getFullYear()][monat]) //gew&auml;hlter Monat in diesem Jahr
    if(monat < (heute.getMonth()+1)){
        jahr = heute.getFullYear() + 1;
    } else{
        jahr = heute.getFullYear();
    }
    
    selektiert = $('tag').options[$('tag').selectedIndex].value;
    
    $('tag').innerHTML = '';
    for(i=1;i<=monate[jahr][monat];i++){
        i>9 ? str_i = String(i) : str_i = '0' + String(i);
        if(selektiert == i){
            option = createDOM('option',{'value':str_i, 'selected':'selected'},str_i);
        } else{
            option = createDOM('option',{'value':str_i},str_i);
        }
        $('tag').appendChild(option);
    }	
}
console.log(tage(4));
