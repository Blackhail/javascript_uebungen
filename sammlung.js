// Zufallszahl - Ganzzahl in bestimmten Bereich ////////////////

var min = 1;
var max = 6;
var number_dec1;
var number_dec2;
var number_int;

number_dec1 = (Math.random() * (max - min + 1)) + min;
document.write(number_dec + '<br>');
number_dec2 = (Math.random() * max) + 1;
document.write(number_dec + '<br>');

number_int = Math.floor( number_dec1 );
document.write(number_int);


// größte Zahl mit Ternäroperator ////////////////

var a = -5;
var b = -2;
var c = -6;
var d = 0;
var e = -1;

var ergebnis = (a > b ) ? a : b ;
ergebnis = (ergebnis > c) ? ergebnis : c ;
ergebnis = (ergebnis > d) ? ergebnis : d ;
ergebnis = (ergebnis > e) ? ergebnis : e ;
document.write(ergebnis);

////////////////////////////////////////////////////////////
/* Würfelspiel */
////////////////////////////////////////////////////////////
/* Folgender Code funktioniert nur wenn Zahl im Bildnamen vorhanden ist. */
var wurf = Math.random(); //=> [0;1[
wurf = wurf * 6; // => [0;6[
wurf = parseInt(wurf); // => [0;5]
wurf = wurf + 1; // => [1;6]
document.write(
  '<img style="width:25%" src="../../../lib/img/wuerfel/wuerfel_' +
    wurf +
    '.gif" alt="Würfel Auge"><hr>'
);

///// simuliert das n-malige Würfeln eines idealen Würfels //////////
function Wuerfeln() {
  
  n = document.f_tab.e_n.value;
  h = [0, 0, 0, 0, 0, 0];
  
  for (i = 1; i <= n; i++) {
     az = 6*Math.random();
     az = Math.round(az + 0.5);
     h[az-1]++;
  }
  
  document.f_tab.e_h1.value = h[0];
  document.f_tab.e_h2.value = h[1];
  document.f_tab.e_h3.value = h[2];
  document.f_tab.e_h4.value = h[3];
  document.f_tab.e_h5.value = h[4];
  document.f_tab.e_h6.value = h[5];
  
  r = [0, 0, 0, 0, 0, 0];
  for (i = 0; i <= 5; i++) {
     r[i] = Math.round(10000*h[i]/n)/10000;
  }
  
  document.f_tab.e_r1.value = r[0];
  document.f_tab.e_r2.value = r[1];
  document.f_tab.e_r3.value = r[2];
  document.f_tab.e_r4.value = r[3];
  document.f_tab.e_r5.value = r[4];
  document.f_tab.e_r6.value = r[5];
  
  var m = Math.max(r[0],r[1],r[2],r[3]);
  var c = Math.round(246/rmax);
  
  document.f_sln.r1.width = r[0]*c;
  document.f_sln.r2.width = r[1]*c;
  document.f_sln.r3.width = r[2]*c;
  document.f_sln.r4.width = r[3]*c;
  document.f_sln.r5.width = r[4]*c;
  document.f_sln.r6.width = r[5]*c;
  
  } 