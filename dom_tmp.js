'use strict';
/* task1: 1 Eigenschaften und Attribute auslesen/schreiben */
function task1() {
  function markLink(clickedLink) {
   
    var marker = document.querySelector('.marker');
   
    if (marker) {
      marker.className = '';
    }
    clickedLink.className = 'marker';
  }

  function generateHTML(clickedLink) {
    var html = 'Die id des geklickten links <b>' + clickedLink.id + '</b><br>';
    html += 'Die Zahl <b>' + clickedLink.id.split('-')[1] + '</b><br>';
    html += 'Der Textknoten: <b>' + clickedLink.innerHTML + '</b><br>';
    html +=
      'Er wurde durch JavaScript mit der Klasse <b>' +
      clickedLink.className +
      '</b> versehen.';
    document.querySelector('#ausgabe').innerHTML = html;
  }

  function handleClick() {
    markLink(this);
    generateHTML(this);
    return false;
  }

  function bindHandler() {
    var elemente = document.querySelectorAll('#einstieg ul a');
    console.log(elemente);
    var element;
    for (var i = 0; i < elemente.length; i++) {
      element = elemente[i];
      element.onclick = handleClick;
    }
  }

  bindHandler();
  document.querySelector('#ausgabe').innerHTML = '';
}

/* task2: 2 Geburtstagsliste */
function task2() {
  function handleSubmit() {
    var inputVorname = document.querySelector('#vorname');
    var inputNachname = document.querySelector('#nachname');
    var inputGeburtstag = document.querySelector('#geburtstag');
    var divFehler = document.querySelector('#fehler');
    var fehler = false;

    divFehler.innerHTML = '';

    if (inputVorname.value.trim() === '') {
      divFehler.innerHTML += 'Vorname darf nicht leer sein<br>';
      fehler = true;
    }
    if (inputNachname.value.trim() === '') {
      divFehler.innerHTML += 'Nachname darf nicht leer sein<br>';
      fehler = true;
    }
    if (inputGeburtstag.value.trim() === '') {
      divFehler.innerHTML += 'Geburtstag darf nicht leer sein<br>';
      fehler = true;
    }

    console.log(divFehler.innerHTML);

    if (fehler === false) {
      var liste = document.querySelector('#liste');
      var newLi =
        '<li>' +
        inputVorname.value.trim() +
        ' ' +
        inputNachname.value.trim() +
        ' ' +
        inputGeburtstag.value.trim() +
        '</li>';
      liste.innerHTML = liste.innerHTML + newLi;
      inputVorname.value = '';
      inputNachname.value = '';
      inputGeburtstag.value = '';
    }
    return false; // Standardverhalten (submit)
  }

  function bindHandler() {
    document.querySelector('#dom form').onsubmit = handleSubmit;
  }

  bindHandler();
}

/* task3: 3 Bildwechsler (beim laden und onmouseover) */
function task3() {
  function generateRandomSrc() {
    var images = ['bild1.jpg', 'bild2.jpg', 'bild3.jpg'];
    var randomImage = images[Math.floor(Math.random() * images.length)];
    return '../../../lib/img/nicht_lustig/' + randomImage;
  }

  function changeImage() {
    this.src = generateRandomSrc();
  }

  function bindHandler() {
    img.onmouseover = changeImage;
  }

  var img = document.querySelector('#bilder img');
  img.src = generateRandomSrc();
  bindHandler();
}

function init() {
  task1();
  task2();
  task3();
}

window.onload = init;









//////////////////////////////////////////////////////////////////////////////
/* return false in der auf das Event refernzierten FN verhindert das Standardverhalten. Bei Hyperlinks die Weiterleitung zum href Ziel und bei Formularen den submit zu action Ziel.


HTML:
<input type="text" id="vorname" name="vorname" placeholder="Max">

DOM: 
<input type="text" id="vorname" name="vorname" placeholder="Max">

JS: 
var input_vorname = document.querySelector('#vorname');

JS-Objekt-Typ-Elmenent, JS-Objekt-Typ-Input-Element. 
Alle Attribut die dieses HTML-Element (input) haben könnte werden im Objekt repräsentiert. Alle Attribute die einen Wert haben, haben auch Werte in der Eigenschaft. Zusätzlich werden spezielle DOM-Eigenschaft hinzugefügt, z.B. innerHTML.



button[type=button] //=> Standardverhalten (schön aussehen!)
button[type=submit] //=> Standardverhalten (submit/Weiterleitung)

input[type=button] //=> Standardverhalten (schön aussehen!)
input[type=submit] //=> Standardverhalten (submit/Weiterleitung)

a[href=http://www.google.de] //=> Standardverhalten (Weiterleitung)
a[href=#] //=> Standardverhalten (Weiterleitung: Springt an Seitenanfang)

 */

// window.onload = function () {
//   'use strict';
//   //Code wird ausgeführt wenn Seite vollständig geladen wurde
//   console.log(document.querySelector('h2'));
// };
