/**
 * rand [Liefert eine Pseudozufallszahl zwischen min und max (inklusive)]
 * @param {number} min [Der niedrigste zurückzugebende Wert]
 * @param {number} max [Der höchste zurückzugebende Wert ]
 * @return {number} [Ein Pseudozufallswert zwischen min und max ]
 */

/* rand ( {number} min , {number} max ) : {number} */
function rand(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function shuffle(array) {
  return array.sort(function () {
    return Math.random() - 0.5;
  });
}

function asc(a, b) {
  return a - b;
}

function desc(a, b) {
  return b - a;
}

function createCaptcha(len) {
  var signs = '0123456789abcdefghijklmnopqrstuvwxyz'.split('');
  if (len > signs.length - 1) {
    len = signs.length - 1;
  }
  shuffle(signs);
  return signs.splice(0, len).join('');
}

/**
 * stripTags [Entfernt HTML-Tags und teilweise reservierte HTML-Zeichen aus einer Zeichenkette]
 * @param {string} html [Zeichenkette mit HTML-Tags]
 * @return {string} [Zeichenkette ohne HTML-Tags und teilweise reservierte HTML-Zeichen]
 */
function stripTags(html) {
  return html.replace(/<[^>]+>/gi, '');
}

/**
 * escapeHTML [Tauscht reservierte HTML-Zeichen gegen Ihre Entitäten aus]
 * @param {string} html [Zeichenkette mit HTML-Tags]
 * @return {string} [Zeichenkette mit Entitäten]
 */
/* TODO: Vereinfachen mit CallBack-FN */
function escapeHTML(html) {
  return html
    .replace(/&/g, '&amp;')
    .replace(/</g, '&lt;')
    .replace(/>/g, '&gt;')
    .replace(/"/g, '&quot;')
    .replace(/\'/g, '&apos;');
}

function removeWhitespaces(s) {
  return s.replace(/\s{2,}/g, ' ');
}

function emptyEl(element) {
  while (element.firstChild) {
    element.firstChild.remove();
  }
}

function createEl(tag, txt) {
  tag = document.createElement(tag);
  if (txt) {
    txt = document.createTextNode(txt);
    tag.appendChild(txt);
  }
  return tag;
}

function insertAfter(newNode, refNode) {
  return refNode.parentNode.insertBefore(newNode, refNode.nextSibling);
}

function fragmentFromString(strHTML) {
  /* https://developer.mozilla.org/en-US/docs/Web/API/Range/createContextualFragment */
  return document.createRange().createContextualFragment(strHTML);
}

function isChecked(collection) {
  var len = collection.length;
  for (var i = 0; i < len; i++) {
    if (collection[i].checked) {
      return true;
    }
  }
  return false;
}

function isBlank(s) {
  return s.trim().length === 0 ? true : false;
}

/**
 * ucfirst [Ersten Buchstaben einer Zeichenkette in Großbuchstabe umwandeln]
 * @param {string} s
 * @return {string} [Die Zeichenkette mit einem Großbuchstaben am Anfang]
 */
function ucfirst(s) {
  return s.slice(0, 1).toUpperCase() + s.slice(1);
}

/**
 * ucwords [Ersten Buchstaben jedes Teilstrings (Wort) in einer Zeichenkette in Großbuchstaben umwandeln]
 * @param {string} s
 * @return {string} [Die Zeichenkette mit einem Großbuchstaben am Anfang jedes Teilstrings (Wort)]
 */
function ucwords(s) {
  var words = s.split(' ');
  for (var i = 0; i < words.length; i++) {
    words[i] = ucfirst(words[i]);
  }
  return words.join(' ');
}

function randomRGB() {
  var rgb = [];
  while (rgb.length < 3) {
    rgb.push(rand(0, 255));
  }
  return 'rgb(' + rgb + ')';
}

function randomHex() {
  return '#' + ((Math.random() * 0xffffff) << 0).toString(16);
}

function componentToHex(c) {
  var hex = c.toString(16);
  return hex.length == 1 ? '0' + hex : hex;
}

function rgbToHex(r, g, b) {
  return '#' + componentToHex(r) + componentToHex(g) + componentToHex(b);
}

function hexToRgb(hex) {
  var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  return result ?
    {
      r: parseInt(result[1], 16),
      g: parseInt(result[2], 16),
      b: parseInt(result[3], 16),
    } :
    null;
}

function ajaxGet(file, fn, method) {
  if (!method) method = 'GET';
  var xhr = new XMLHttpRequest();
  xhr.open(method, file, true);
  xhr.onreadystatechange = function () {
    if (xhr.readyState === 4 && xhr.status === 200) {
      fn(xhr.responseText);
    }
  };
  xhr.send();
}
/**
 * ajaxGETJSON [GET Anfrage auf JSON-Datei gibt automatisch JS-Objekt zurück]
 * @param {string} file [Pfad zur Datei die angefordert wird]
 * @param {function} fn [FN die die Antwort vom Server verarbeitet]
 * return undefined
 */
function ajaxGetJSON(file, fn, method) {
  if (!method) method = 'GET';
  var xhr = new XMLHttpRequest();
  xhr.open(method, file, true);
  xhr.onreadystatechange = function () {
    if (xhr.readyState === 4 && xhr.status === 200) {
      fn(JSON.parse(xhr.responseText));
    }
  };
  xhr.send();
}

function ajaxGetXML(file, fn, method) {
  if (!method) method = 'GET';
  var xhr = new XMLHttpRequest();
  xhr.open(method, file, true);
  xhr.onreadystatechange = function () {
    if (xhr.readyState === 4 && xhr.status === 200) {
      fn(xhr.responseXML);
    }
  };
  xhr.send();
}

/**
 * getJS
 * @param url: URL of external file
 * @param implementationCode: the code to be called from the file
 * @param location:  location to insert the <script> element
 */
function getJS(url, implementationCode, location) {
  var scriptTag = document.createElement('script');
  scriptTag.src = url;
  scriptTag.onload = implementationCode;
  scriptTag.onreadystatechange = implementationCode;
  location.appendChild(scriptTag);
}

function randomNameGenerator() {
  var firstnames = [
    'Adam',
    'Alex',
    'Aaron',
    'Ben',
    'Carl',
    'Dan',
    'David',
    'Edward',
    'Fred',
    'Frank',
    'George',
    'Hal',
    'Hank',
    'Ike',
    'John',
    'Jack',
    'Joe',
    'Larry',
    'Monte',
    'Matthew',
    'Mark',
    'Nathan',
    'Otto',
    'Paul',
    'Peter',
    'Roger',
    'Roger',
    'Steve',
    'Thomas',
    'Tim',
    'Ty',
    'Victor',
    'Walter',
  ];

  var lastnames = [
    'Anderson',
    'Ashwoon',
    'Aikin',
    'Bateman',
    'Bongard',
    'Bowers',
    'Boyd',
    'Cannon',
    'Cast',
    'Deitz',
    'Dewalt',
    'Ebner',
    'Frick',
    'Hancock',
    'Haworth',
    'Hesch',
    'Hoffman',
    'Kassing',
    'Knutson',
    'Lawless',
    'Lawicki',
    'Mccord',
    'McCormack',
    'Miller',
    'Myers',
    'Nugent',
    'Ortiz',
    'Orwig',
    'Ory',
    'Paiser',
    'Pak',
    'Pettigrew',
    'Quinn',
    'Quizoz',
    'Ramachandran',
    'Resnick',
    'Sagar',
    'Schickowski',
    'Schiebel',
    'Sellon',
    'Severson',
    'Shaffer',
    'Solberg',
    'Soloman',
    'Sonderling',
    'Soukup',
    'Soulis',
    'Stahl',
    'Sweeney',
    'Tandy',
    'Trebil',
    'Trusela',
    'Trussel',
    'Turco',
    'Uddin',
    'Uflan',
    'Ulrich',
    'Upson',
    'Vader',
    'Vail',
    'Valente',
    'Van Zandt',
    'Vanderpoel',
    'Ventotla',
    'Vogal',
    'Wagle',
    'Wagner',
    'Wakefield',
    'Weinstein',
    'Weiss',
    'Woo',
    'Yang',
    'Yates',
    'Yocum',
    'Zeaser',
    'Zeller',
    'Ziegler',
    'Bauer',
    'Baxster',
    'Casal',
    'Cataldi',
    'Caswell',
    'Celedon',
    'Chambers',
    'Chapman',
    'Christensen',
    'Darnell',
    'Davidson',
    'Davis',
    'DeLorenzo',
    'Dinkins',
    'Doran',
    'Dugelman',
    'Dugan',
    'Duffman',
    'Eastman',
    'Ferro',
    'Ferry',
    'Fletcher',
    'Fietzer',
    'Hylan',
    'Hydinger',
    'Illingsworth',
    'Ingram',
    'Irwin',
    'Jagtap',
    'Jenson',
    'Johnson',
    'Johnsen',
    'Jones',
    'Jurgenson',
    'Kalleg',
    'Kaskel',
    'Keller',
    'Leisinger',
    'LePage',
    'Lewis',
    'Linde',
    'Lulloff',
    'Maki',
    'Martin',
    'McGinnis',
    'Mills',
    'Moody',
    'Moore',
    'Napier',
    'Nelson',
    'Norquist',
    'Nuttle',
    'Olson',
    'Ostrander',
    'Reamer',
    'Reardon',
    'Reyes',
    'Rice',
    'Ripka',
    'Roberts',
    'Rogers',
    'Root',
    'Sandstrom',
    'Sawyer',
    'Schlicht',
    'Schmitt',
    'Schwager',
    'Schutz',
    'Schuster',
    'Tapia',
    'Thompson',
    'Tiernan',
    'Tisler',
  ];

  return (
    firstnames[rand(0, firstnames.length - 1)] +
    ' ' +
    lastnames[rand(0, lastnames.length - 1)]
  );
}