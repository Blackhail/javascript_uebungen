(function (window, document) {
'use strict';

  function createInfobox(playingCard) {
      var infoText = document.getElementById('infotext');
      emptyEl(infoText);
      var infoMain = document.getElementById('info');

      var img = document.createElement('img');
      img.setAttribute('src', 'lib/img/magic/preview/' + playingCard['image']);
      img.setAttribute('title', playingCard['title']);
      img.setAttribute('alt', playingCard['alt']);

      var data = playingCard['data'];
      var ul = document.createElement('ul');
      var h2 = document.createElement('h2');
      var h2Text = document.createTextNode(playingCard['headline']);
      var liForm = document.createElement('li');
      var input = document.createElement('input');
      input.setAttribute('value' , '1')
      input.setAttribute('type' , 'text')
      var button = document.createElement('button');
      button.setAttribute('type' , 'button')
      var buttonText = document.createTextNode('In den Warenkorb');
      button.appendChild(buttonText);
      liForm.appendChild(input);
      liForm.appendChild(button);
      h2.appendChild(h2Text);
      infoMain.appendChild(h2);
      ul.appendChild(h2);


      for (var i = 0; i < data.length; i++) {
          var li = document.createElement('li');
          var liText = document.createTextNode(data[i]);
          li.appendChild(liText);
          ul.appendChild(li);
      }

      ul.appendChild(liForm);
      infoText.appendChild(img);
      infoText.appendChild(ul);
  }

  function handleClick(e) {
      e.preventDefault();
      var cards = document.getElementById('preview').getElementsByTagName('a');
      var index;
      for (var i = 0; i < cards.length; i++) {
          if (cards[i] === this) {
            index = i;
          }
      }
      ajaxGetJSON('lib/data/aufgabe1_data.json', function (data) {
        createInfobox(data[index]);
      });
  }

  function bindHandler() {
    var cards = document.getElementById('preview').getElementsByTagName('a');
    for (var i = 0; i < cards.length; i++) {
        cards[i].addEventListener('click', handleClick);
    }
  }

  window.addEventListener('load', function () {
      bindHandler();
  });

})(window, document);