console.log('test');
(function (window, document) {
    'use strict';

    function createInfobox(deck) {

        var deckInfo = document.getElementById('right');
        emptyEl(deckInfo);
        var deckCards = deck['Karten'];

        var img = document.createElement('img');
        img.setAttribute('src', 'lib/img/magic/deck_ansicht/' + deck['Bild']['src']);
        img.setAttribute('alt', deck['Bild']['alt']);
        img.setAttribute('title', deck['Bild']['title']);

        var h2 = document.createElement('h2');
        h2.appendChild(document.createTextNode(deck['Deckname']));
        var h3 = document.createElement('h3');
        h3.appendChild(document.createTextNode('Karten'));
        var ul = document.createElement('ul');

        for (var i = 0; i < deckCards.length; i++) {
            var li = document.createElement('li');
            var liText = document.createTextNode(deckCards[i]);
            li.appendChild(liText);
            ul.appendChild(li);
        }

        var li = document.createElement('li');
        var p = document.createElement('p');
        var a = document.createElement('a');
        var aText = document.createTextNode(deck['Deckname']);
        a.setAttribute('href', deck['Link']);
        a.append(aText);
        p.appendChild(document.createTextNode('Kaufen: '));
        p.appendChild(a);

        deckInfo.appendChild(h2);
        deckInfo.appendChild(img);
        deckInfo.appendChild(h3);
        deckInfo.appendChild(ul);
        deckInfo.appendChild(p);
    }

    function handleClick(e) {
        e.preventDefault();
        var cards = document.getElementById('auswahl').getElementsByTagName('a');
        var index;
        for (var i = 0; i < cards.length; i++) {
            if (cards[i] === this) {
                index = i;
            }
        }

        ajaxGetJSON('lib/data/aufgabe2_data.json', function (data) {
            createInfobox(data[index]);
        });
    }

    function bindHandler() {
        var cards = document.getElementById('auswahl').getElementsByTagName('a');
        for (var i = 0; i < cards.length; i++) {
            cards[i].addEventListener('click', handleClick);
        }
    }

    window.addEventListener('load', function () {
        bindHandler();
    });

})(window, document);